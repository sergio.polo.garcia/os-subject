1. ¿Qué hace el comando date? Mira man. Explica date +%s%3N

Muestra la hora actual o establece la fecha del sistema.
 La opción +%s%3N muestra los segundos y milisegundos (%3N trunca los nano segundos).


2. ¿Qué hace tarea.sh?

Esperar y luego, imprimir un texto con una duracion determinada.


3. Explica usando los tiempos de tarea el tiempo que te da time en el caso de secuencial.

Tarea1 (4.006 seg) 
Tarea2 (3.003 seg) 
Tarea3 (2.003 seg) 
Tarea4 (1.004 seg) 
Nos arroja un resultado de 10 segundos aproximadamente.


4. Explica usando los tiempos de tarea el tiempo que te da time en el caso de paralelo.

Tarea4 (1.004 seg) 
Tarea3 (2.003 seg) 
Tarea2 (3.003 seg) 
Tarea1 (4.006 seg) 
Nos arroja un resultado de 4 segundos aproximadamente.


5. Explica la siguiente línea. time echo 4 Tarea1 3 Tarea2 2 Tarea3 1 Tarea4 | xargs -n 2 -P 4 ./tarea.sh

Time echo --> Nos da el tiempo que tardan las tareas. 
4 Tarea1 3 Tarea2 2 Tarea3 1 Tarea4 --> Tareas a realizar y el tiempo que tardaran en ejecutarse.
xargs --> realiza procesos en paralelo. 
-n 2 --> Señala las tareas han de ir de dos en dos.
-P 4 --> Indica cuantos procesos van a ir de forma simultánea 4 en este caso. 
./tarea.sh --> Script a ejecutar.


7.Explica la opción -j

Ejecuta el numero de procesos indicados en paralelo.